# Gitlab Templates

Some of my personal Gitlab templates

## How to use

### In Gitlab.com

```yaml
include:
    - project: 'mathiusd/gitlab-templates'
      file: '<fileToInclude>.yml'
```

### In other Gitlab Instance

```yaml
include:
    - remote: 'https://gitlab.com/mathiusd/gitlab-templates/-/raw/master/<fileToInclude>.yml'
```
